import {Button, Form, Container, Row, Col} from 'react-bootstrap';
import {Fragment} from 'react';
import {useContext, useState, useEffect} from 'react'
import {Navigate, useNavigate, Link} from "react-router-dom"
import Swal from 'sweetalert2'

import UserContext from '../UserContext.js'

export default function Login(){

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const navigate = useNavigate();

	const {user, setUser} = useContext(UserContext)
	const [isActive, setIsActive] = useState(false)


	useEffect(() => {
		if(email !== "" && password !== ""){
			setIsActive(true)
		}
		else {
			setIsActive(false)
		}
	}, [email, password])


	/*useEffect(() => {
		if(user.isAdmin == true){
			setIsAdmin(true)
		}
		else {
			setIsAdmin(false)
		}
	}, [user])*/


	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
		
			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}


	const checkUser = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/user/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(result => result.json())
		.then(data => {
			
			data.isAdmin ?
			Swal.fire({
				title: "Authentication Successful!",
				icon: "success",
				text: "Welcome back, Admin User!",
				showConfirmButton: true
			})
			:
			Swal.fire({
				title: "Welcome to Clothing!",
				icon: "success",
				text: "Happy shopping!",
				showConfirmButton: false,
				timer: 2800
			})
			
		})
	}


	function login(event){
		event.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === false) {
				Swal.fire({
					title: "Authentication failed!",
					icon: "error",
					text: "Please try again!",
					showConfirmButton: false,
					timer: 2000
				})
			}
			else{
				localStorage.setItem('token', data.auth)
				retrieveUserDetails(localStorage.getItem("token"))
				checkUser(localStorage.getItem("token"))
				
				navigate("/")
			}
		})
	}




	return(

		user ?
		<Navigate to = "/*"/>
		:
		<Container className="fluid pb-5 mb-5">
		<Fragment>
		<h1 className = "text-center mt-5 pt-5">Sign In</h1>
		<Row>
			<Col className = "col-md-6 mx-auto">
				<Form className = 'mt-5 mx-5' onSubmit = {event=> login(event)}>
				      <Form.Group className="mb-3" controlId="formBasicEmail">
				        <Form.Label>Email address</Form.Label>
				        <Form.Control 
					        type="email" 
					        placeholder="Enter email" 
					        value ={email}
					        onChange = {event => setEmail(event.target.value)}
					        required
					        />
				        <Form.Text className="text-muted">
				        </Form.Text>
				      </Form.Group>

				      <Form.Group className="mb-3" controlId="formBasicPassword">
				        <Form.Label>Password</Form.Label>
				        <Form.Control 
				        	className = "mb-2"
					        type="password" 
					        placeholder="Password" 
					        value ={password}
					        onChange = {event => setPassword(event.target.value)}
					        required
					        />
							<Form.Text className="text-muted">
								Not Registered yet? Sign-up <Link to = '/register'>here</Link>.
							</Form.Text>
				      </Form.Group>


				      {
				      	isActive ?
				      	<Button variant="primary" type="submit">
				        Login
				      </Button>
				      :
				      <Button variant="secondary" type="submit" disabled>
				        Login
				      </Button>
				      
				      }

				      
				    </Form>
			    </Col>
			</Row>
		 </Fragment>
		 </Container>
		)
}