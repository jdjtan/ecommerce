import Accordion from 'react-bootstrap/Accordion';
import { Container } from "react-bootstrap";
import { useEffect, useState } from "react";
import Button from 'react-bootstrap/Button';
import {Link} from "react-router-dom"

import OrdersAccordion from '../components/AdminDashboard/OrdersAccordion.js'
import ScrollButton from '../components/ScrollButton/ScrollButton.js';


function AdminDashboard() {

  const [allOrders, setAllOrders] = useState([]);
  // const [totalAmount, setTotalAmount] = useState(0);


  useEffect(() =>{
    // fetch all orders of all users
    fetch(`${process.env.REACT_APP_API_URL}/order/viewAll`, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      })
    .then(result => result.json())
    .then(data => {
      
    if(data !== false){
      setAllOrders(data.map(order => {
        return(
          <OrdersAccordion key = {order._id} allOrderProp = {order}/>
        )
      }))
    }

    })

  }, [])


  return (
    <div className= "pt-5 mt-5 pb-5 mb-5">
      <Container className = "fluid mx-auto col-md-8">
        <h1 className = "text-center">Admin Dashboard</h1>
        <div className="mx-auto text-center">
        <Button as = {Link} to = "/AdminDashboard/AddNewProduct" variant="primary" className="me-2">Add Product</Button>
        {' '}
        <Button as = {Link} to = "/AdminDashboard/ShowAllProducts" variant="secondary" className="ms-2">Show All Products</Button>
        </div>
        <h3 className = "text-left mt-2 mb-3">Users' Orders</h3>
        <Accordion>
          {allOrders}
        </Accordion>
      </Container>
      <ScrollButton />
    </div>
  )
}

export default AdminDashboard;