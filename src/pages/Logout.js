import {Navigate} from "react-router-dom"
import {useContext, useEffect} from "react"
import Swal from 'sweetalert2'

import UserContext from "../UserContext.js"


export default function Logout() {

	const {unSetUser, setUser} = useContext(UserContext);

	useEffect(() =>{
		unSetUser()
		setUser(null)
	}, [])

	const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true
})

Toast.fire({
  icon: 'success',
  title: 'Signed out successfully!'
})
	

	return(
		<Navigate to = "/" />
		)
}