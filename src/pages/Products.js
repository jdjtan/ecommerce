import React from "react";
import 'animate.css';
import ProductsCard from '../components/Products/ProductsCard.js'
import {Fragment, useEffect, useState} from 'react';
import {Row, Container} from 'react-bootstrap';
import ScrollButton from '../components/ScrollButton/ScrollButton.js';

import background from '../images/productsview.jpg'

export default function Products() {

	const [products, setProducts] = useState([])

	useEffect(() =>{
		// fetch all ACTIVE products
		fetch(`${process.env.REACT_APP_API_URL}/product/catalogue`)
		.then(result => result.json())
		.then(data => {
			// console.log(data);

			setProducts(data.map(product => {
				return(
					<ProductsCard key = {product._id} productProp = {product}/>
				)
			}))
		})

	}, [])

	return(
		<Fragment>
		<div style={{ 
			backgroundImage: `url(${background})`,
			 backgroundRepeat: 'no-repeat',
		}}>
			<h1 className = "text-center mt-5 pt-5">Catalogue</h1>
				<Container className="fluid">
					<Row className = "mt-5">
						{products}
					</Row>
				</Container>
				<ScrollButton />
		</div>
		</Fragment>
		)
}