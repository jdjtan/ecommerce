import {Row , Col, Container, Figure} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';

import deletebutton_black from '../../images/deletebutton_black.png'
// import {Link} from 'react-router-dom'
// import {useState, useEffect, useContext} from 'react'

// import UserContext from "../UserContext.js"

export default function OrdersRow({orderProp}){

	const {productId, productName, size, quantity, price} = orderProp

	return(
		<tr>
			<td className = "text-center"><img src = "data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_186696519cd%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_186696519cd%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22100.25%22%20y%3D%2297.44000034332275%22%3EImage%20cap%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" height="50" width="50"></img></td>
			<td>{productName}</td>
			<td className = "text-center">{size}</td>
			<td className = "text-center">{quantity}</td>
			<td className = "text-center">{price}</td>
			<td className = "text-center my-auto">
			<Button variant="light" size="sm"><img src = {deletebutton_black} style={{ width: 25, height: 25 }}/></Button>

			</td>
		</tr>
	)
}