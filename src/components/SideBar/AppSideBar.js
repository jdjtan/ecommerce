import React, { useContext } from 'react'
import {SidebarContainer, Icon, CloseIcon, SidebarWrapper, SidebarMenu, SidebarLink, SideBtnWrap, SidebarRoute, SidebarContainer2, SidebarRoute2} from './SideBarElements.js'
import {Button} from 'react-bootstrap';
import { NavLink } from 'react-router-dom'
import Badge from 'react-bootstrap/Badge';

import UserContext from "../../UserContext.js"

const Sidebar = ({isOpen, toggle}) => {
	const {user} = useContext(UserContext);
	

	return(
		
		user ?
		<SidebarContainer2 isOpen={isOpen} onClick={toggle}>
			<Icon onClick={toggle}>
				<CloseIcon />
			</Icon>
			<SidebarWrapper>
				<SidebarMenu>
					<SidebarLink as = {NavLink} to = "/newarrivals" onClick={toggle}>
					Discover<Badge bg="secondary ms-1">New</Badge>
					</SidebarLink>
					<SidebarLink as = {NavLink} to = "/catalogue" onClick={toggle}>
					Products
					</SidebarLink>
					{
					user.isAdmin ?
						<SidebarLink as = {NavLink} to = "/AdminDashboard" onClick={toggle}><Button variant="primary">ADMIN DASHBOARD</Button></SidebarLink>
						:
						<SidebarLink as = {NavLink} to = "/cart" onClick={toggle}>
						My Cart <Badge bg="secondary mx-2">0</Badge>
						</SidebarLink>
					}
				</SidebarMenu>
				<SideBtnWrap>
					<SidebarRoute2 as = {NavLink} to = "/logout" onClick={toggle}>Sign Out</SidebarRoute2>
				</SideBtnWrap>
			</SidebarWrapper>
		</SidebarContainer2>
		:
		<SidebarContainer isOpen={isOpen} onClick={toggle}>
			<Icon onClick={toggle}>
				<CloseIcon />
			</Icon>
			<SidebarWrapper>
				<SidebarMenu>
					<SidebarLink as = {NavLink} to = "/newarrivals" onClick={toggle}>
					Discover<Badge bg="secondary ms-1">New</Badge>
					</SidebarLink>
					<SidebarLink as = {NavLink} to = "/catalogue" onClick={toggle}>
					Products
					</SidebarLink>
					<SidebarLink as = {NavLink} to = "/register" onClick={toggle}>
					Sign Up
					</SidebarLink>
				</SidebarMenu>
				<SideBtnWrap>
					<SidebarRoute as = {NavLink} to = "/login" onClick={toggle}>Sign In</SidebarRoute>
				</SideBtnWrap>
			</SidebarWrapper>
		</SidebarContainer>
		
	)
}

export default Sidebar