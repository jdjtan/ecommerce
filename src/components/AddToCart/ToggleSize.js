import React, { useState } from 'react';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';


export default function ToggleButtonExample() {
  const [radioName, setRadioName] = useState('Small');

  const radios = [
    { name: 'Small', value: '1' },
    { name: 'Medium', value: '2' },
    { name: 'Large', value: '3' },
    { name: 'XLarge', value: '4' },
  ];


  

  return (
    <>
      
      <ButtonGroup>
        {radios.map((radio, idx) => (
          <ToggleButton
            key={idx}
            id={`radio-${idx}`}
            type="radio"
            variant={'outline-secondary'}
            name="radio"
            value={radio.name}
            checked={radioName}
            onChange={(e) => setRadioName(e.currentTarget.name)}
          >
            {radio.name}
          </ToggleButton>
        ))}
      </ButtonGroup>
    </>
  );
}
