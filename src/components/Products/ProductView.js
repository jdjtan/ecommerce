import {useState, useEffect, useContext} from 'react'
import {Container, Row, Col, Card, Button} from 'react-bootstrap'
import {useParams, useNavigate, Link} from 'react-router-dom'
import UserContext from "../../UserContext.js"
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import ToggleButton from 'react-bootstrap/ToggleButton';
import Swal from 'sweetalert2'

// import IncDecCounter from "../IncrementDecrementCounter/IncDecCounter.js"
// import ToggleSize from "../AddToCart/ToggleSize.js"
import SuccessNotif from "./SuccessNotif.js"



export default function ProductView() {

	const [name, setName] = useState('');
	// const [productId, setProductId] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [size, setSize] = useState('')
	const navigate = useNavigate()

	const {user} = useContext(UserContext)

	const {productId} = useParams()

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/details/${productId}`)
		.then(result => result.json())
		.then(data => {
			
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price)
			// setProductId(data._id)
		})

	}, [productId])



	function addToCart(event){
		event.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/order/addToCart`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				size: radioValue,
				quantity: num
			})
		})
		.then(result => result.json())
		.then(data => {
			if(data === true) {
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 2200
				})

				Toast.fire({
				  icon: 'success',
				  title: 'Item Successfully Added to Cart!'
				})

			}
			else{
				const Toast = Swal.mixin({
				  toast: true,
				  position: 'top-end',
				  showConfirmButton: false,
				  timer: 2200
				})

				Toast.fire({
				  icon: 'error',
				  title: 'Failed to Add to Cart'
				})
			}
		})
	}

	// Sizes Radio
	const [checked, setChecked] = useState(false);
	const [radioValue, setRadioValue] = useState('Small');
	// const [radioName, setRadioName] = useState('Small');

	const radios = [
	{ name: 'Small', value: 'Small' },
	{ name: 'Medium', value: 'Medium' },
	{ name: 'Large', value: 'Large' },
	{ name: 'XLarge', value: 'XLarge' },
	];


	// IncDecCounter
	 let [num, setNum]= useState(1);
	 let incNum =()=>{
	   if(num<99)
	   {
	   setNum(Number(num)+1);
	   }
	 };
	 let decNum = () => {
	    if(num>1)
	    {
	     setNum(num - 1);
	    }
	 }
	let handleChange = (e)=>{
	  setNum(e.target.value);
	 }




	return(
		<Container className="mt-5 pt-5 pb-5 mb-5">
		<Button as = {Link} to = "/catalogue" variant="secondary" className="m-3">Back</Button>
            <Row>
                <Col lg={{ span: 6, offset: 3 }}>
                    <Card>
                        <Card.Body className="text-center">
                            <Card.Title><h2>{name}</h2></Card.Title>
                            <div className= "text-center m-3 mx-auto col-sm-5"><Card.Img variant="top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_18663ba2559%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_18663ba2559%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22108.53125%22%20y%3D%2297.44000034332275%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" /></div>
                       
                            <h5><Card.Text className="text-muted">{description}</Card.Text></h5>
                            {/*<Card.Subtitle className="text-muted">Price:</Card.Subtitle>*/}
                            <h4><Card.Text>₱{price}</Card.Text></h4>
                     
	                            {
	                            user ?
	                            	user.isAdmin ?
	                            	""
	                            	:
	                            <div>
	                            <ButtonGroup>
	                                    {radios.map((radio, idx) => (
	                                      <ToggleButton
	                                        key={idx}
	                                        id={`radio-${idx}`}
	                                        type="radio"
	                                        variant={'outline-secondary'}
	                                        name="radio"
	                                        value={radio.value}
	                                        checked={radioValue === radio.value}
	                                        onChange={(e) => setRadioValue(e.currentTarget.value)}
	                                      >
	                                        {radio.name}
	                                      </ToggleButton>
	                                    ))}
	                            </ButtonGroup>

	                            <>
		                            <div className="col-md-3 col-sm-4 col-xl-2 col-4 mx-auto m-2">
			                            <div class="input-group">
				                            <div class="input-group-prepend">
				                            	<button class="btn btn-outline-primary" type="button" onClick={decNum}>-</button>
				                            </div>
				                            	<input type="text" class="form-control" value={num} onChange={handleChange}/>
				                            <div class="input-group-prepend">
				                            	<button class="btn btn-outline-primary" type="button" onClick={incNum}>+</button>
				                            </div>
			                            </div>
		                            </div>
	                            </>
	                            </div>
	                            :
	                            ""
								}

	                            {
	                            user ?
	                            	user.isAdmin ?
	                            	<Button variant="secondary" disabled>
									Add to Cart
									</Button>
	                            	:
	                            <Button variant="primary" onClick= {event=> addToCart(event)} style={{background: '#01bf71', border: 'none'}}>Add to Cart</Button>
	                            :
	                            <Button variant="secondary" disabled>
	                            Add to Cart
	                            </Button>
								}
								{
	                            user ?
	                            	user.isAdmin ?
	                            	<Card.Subtitle className="text-muted my-1">Only NON-ADMIN users are allowed to Add to Cart</Card.Subtitle>
	                            	:
	                            ""
	                            :
	                            <Card.Subtitle className="text-muted my-1"><Link to = '/login'>Sign In</Link> to be able to Add to Cart</Card.Subtitle>
								}
							
							
                        </Card.Body>        
                    </Card>
                </Col>
            </Row>
		</Container>
	)
}