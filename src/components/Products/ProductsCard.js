import {Row , Col, Container} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import {Link} from 'react-router-dom'
// import {useState, useEffect, useContext} from 'react'

// import UserContext from "../UserContext.js"

export default function ProductsCard({productProp}){

	const {_id, name, description, price} = productProp
	// const [isDisabled, setIsDisabled] = useState(false);

	return(
				<Col className = "col-xl-4 mx-auto my-5">
					<Card style={{ width: '25rem' }} className = "mx-auto">
						<Card.Img variant="top" src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22286%22%20height%3D%22180%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20286%20180%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_18663ba2559%20text%20%7B%20fill%3A%23999%3Bfont-weight%3Anormal%3Bfont-family%3Avar(--bs-font-sans-serif)%2C%20monospace%3Bfont-size%3A14pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_18663ba2559%22%3E%3Crect%20width%3D%22286%22%20height%3D%22180%22%20fill%3D%22%23373940%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22108.53125%22%20y%3D%2297.44000034332275%22%3E286x180%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E" />
						<Card.Body>
							<Card.Title className = "text-center"><strong>{name}</strong></Card.Title>
							<Card.Text className="text-muted">{description}</Card.Text>
							{/*<Card.Subtitle className="text-muted">Price:</Card.Subtitle>*/}
							<Card.Text><span className="text-warning">₱ {price}</span></Card.Text>
							<Button as = {Link} to = {`/product/${_id}`} style={{background: '#01bf71', border: 'none'}}>See details</Button>
						</Card.Body>
					</Card>
				</Col>
	)
}