import AllProductsList from './AllProductsList.js'
import EditProductModal from './EditProductModal.js'

import { Container, Table } from "react-bootstrap";
import { useEffect, useState, useContext } from "react";
import Button from 'react-bootstrap/Button';
import {Link} from "react-router-dom"
import ScrollButton from '../ScrollButton/ScrollButton.js';

import ModalToggle from "./ModalToggle.js"
import UserContext from '../../UserContext.js'

function ShowAllProducts() {
	const {user, setUser} = useContext(UserContext);
	const [products, setProducts] = useState([])
	const [cartItems, setCartItems] = useState([])

	
	useEffect(() =>{
		// fetch ALL products
		fetch(`${process.env.REACT_APP_API_URL}/product/allProducts`)
		.then(result => result.json())
		.then(data => {
			setProducts(data.map(product => {
				return(
					<AllProductsList key = {product._id} productProp = {product}/>
				)
			}))
		})

	}, [])


	useEffect(() =>{
		fetch(`${process.env.REACT_APP_API_URL}/product/allProducts`)
		.then(result => result.json())
		.then(data => {
			setCartItems(data.map(items => {
				return(
					<ModalToggle key_2 = {items._id} order = {items}/>
				)
			}))
		})

	}, [])


	return(

		<div className= "pt-5 mt-5 pb-5 mb-5">
			<Container className = "fluid mx-auto col-md-10">
				<h1 className = "text-center">Admin Dashboard</h1>
				<div className="mx-auto text-center">
				<Button as = {Link} to = "/AdminDashboard/AddNewProduct" variant="primary" className="me-2">Add Product</Button>
        		{' '}
        		<Button as = {Link} to = "/AdminDashboard/ShowAllProducts" variant="secondary" className="ms-2">Show All Products</Button>
				</div>
				<h3 className = "text-left mt-2 mb-3">All Products</h3>
				<Table bordered hover responsive="sm">
	              <thead className="text-center">
	                <tr>
	                  <th>Name</th>
	                  <th>Description</th>
	                  <th>Price</th>
	                  <th>Active</th>
	                  <th>Action</th>
	                </tr>
	              </thead>
	              <tbody>
					{products}
	              </tbody>
	            </Table>
			</Container>
			<ScrollButton />
		</div>

	)
}

export default ShowAllProducts;