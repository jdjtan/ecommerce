import Card from 'react-bootstrap/Card';
import { Fragment } from "react";
import { Container } from "react-bootstrap";
import Button from 'react-bootstrap/Button';
import {Link} from 'react-router-dom'

function NewArrivalsCarousel({productProp}) {

  const {_id, name, description, price, isActive, createdOn, __v} = productProp

  return (
      <Container className="fluid m-0">
      <Card className="bg-dark text-white m-4">
      
      <Card.Img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTiJnVC9SM7nik14ClyCTsq1iLiDMClNqNqBg&usqp=CAU" alt="Card image" />
      
      <Card.ImgOverlay>
        <Card.Title><h2>{name}</h2></Card.Title>
        <h5><Card.Text>{description}</Card.Text></h5>
        <div className="mt-4">
        <Button as = {Link} to = {`/product/${_id}`} style={{background: '#01bf71', border: 'none'}}>Shop Now</Button>
        </div>
      </Card.ImgOverlay>
    </Card>
    </Container>
  );
}

export default NewArrivalsCarousel